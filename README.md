Backbone.jsとRails3でタスク管理アプリを作ってみました。

####【前提条件】
* フレームワーク: Rails
* DB: SQLite
* クライント側の実装は、backbone.js

#### 【使い方】
rake db:setup
rails s

#### 【懸念点】
* JSのライブラリー管理
 - bowerで管理する
 http://tnakamura.hatenablog.com/entry/2013/07/02/194508

* JSのテンプレートエンジンに何を使用するか？
 - EJS(今回、採用!)
 - Handler.js

* Backbone.jsにおけるrelationをどうやって実現するか？

* BackboneとRailsのモデルの重複コードをどうやって減らすか？

* BackboneでどうやってリンクURLを管理するか？

→Routerオブジェクトを使う


####　【注意事項】
* SQLiteは、booleanの型に対応していないため、integerで表現すること

####　【残作業】
* ユニットテスト
* インテグレーションテスト
* パッケージ管理

---------------------

#####個人的に興味があること

基本的なアニメーション

* ツールチップス
* ポップアップ
* モダル
* トランジション


