describe('#task', function(){
  describe('#カラムにつて' ,function(){
    beforeEach(function(){
      this.task = new Todo.Models.Task({ title: 'タイトル', status: 0});
    });
    it('「タイトル」と出力されること', function(){
      expect(this.task.get('title')).toEqual('タイトル');
    });
    it('「未完了」ステータスであること', function(){
      expect(this.task.status_display()).toEqual('未完了');
    });
  });
});
