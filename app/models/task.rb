# == Schema Information
#
# Table name: tasks
#
#  id         :integer          not null, primary key
#  title      :string(64)       not null
#  end_date   :date
#  status     :integer          default(0), not null
#  deleted    :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Task < ActiveRecord::Base

  STATUS_NEW = 0
  STATUS_IN_PROGRESS = 1
  STATUS_DONE = 2

  DELETED = 1

  scope :active, where(deleted: false)

  def as_json(options = {})
    super(options.merge(:only => [ :id, :title, :end_date, :status, :deleted ]))
  end

end
