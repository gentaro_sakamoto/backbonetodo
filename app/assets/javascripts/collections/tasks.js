Todo.Collections.Tasks = Backbone.Collection.extend({

  model: Todo.Models.Task,

  url: '/tasks',

  selected: function(){
    return this.filter(function(task){return task.get('selected')});
  },

  selected_any: function(){
    return this.some(function(task){return task.get('selected')})
  },

  orderBy: function(key, type){
    this.comparator = function(model){
      var value = model.get(key);
      if(type == 'dsc'){
        if( typeof value == 'string'){
          value = value.split("").map(function(letter){
            return String.fromCharCode(-(letter.charCodeAt(0)));
          });
        }else{
          return -value;
        }
      }
      return value;
    }
    this.sort();
  }


});
