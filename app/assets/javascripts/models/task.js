Todo.Models.Task = Backbone.Model.extend({
  modelName: 'task',
  url: '/tasks',
  status_display: function(){
    return Todo.Models.Task.STATUSES[this.get('status')];
  }

},{
  STATUSES: {
    0: '未完了',
    1: '完了'
  }
});
