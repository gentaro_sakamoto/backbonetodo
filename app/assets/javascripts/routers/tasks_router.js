Todo.Routers.Tasks = Backbone.Router.extend({
  routes: {
    '': 'index'
  },
  index: function() {
    var tasks = new Todo.Collections.Tasks();
    tasks.fetch( {reset: true});

    //一覧
    var view = new Todo.Views.TasksList({ collection: tasks });
    $('#list').append(view.render().$el);

    //フォーム
    var view = new Todo.Views.TasksFormRegist({ collection: tasks});
    $('#form').append(view.render().$el);

    //検索ボックス
    var view = new Todo.Views.TasksSearch({ collection: tasks});

  }
});
