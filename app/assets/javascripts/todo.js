var Todo = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    new Todo.Routers.Tasks();
    Backbone.history.start();
  }
};

$(function(){
  Todo.initialize()
});

