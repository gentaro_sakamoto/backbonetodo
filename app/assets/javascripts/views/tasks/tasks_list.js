Todo.Views.TasksList = Backbone.View.extend({
  el: '#list',
  initialize: function(){
    this.collection.on('reset', this.render, this);
    this.collection.on('sort', this.render, this);
    this.collection.on('remove', this.render, this);
    this.collection.on('add', this.render, this);
    this.collection.on('change:selected', this.render, this);
  },
  events: {
    "click #delete-selected": "deleteSelected",
    "click #task-list th a": "sort"
  },

  template: function(options) {
    return JST['tasks/list'](options);
  },

  render : function() {
    var self = this;

    this.$el.html(this.template({tasks: this.collection}));
    if (this.collection.size()){
      this.collection.each(function (task){
        var taskView = new Todo.Views.TasksRecord(
          { model: task,
            className: task.get('selected')?'selected':''
          }
        );
        self.$("#task-list").append(taskView.render().el);
      });
    }
    return this;
  },

  deleteSelected: function(e){
    this.collection.selected().map(function(task){task.destroy()})
  },

  sort: function(e){
    e.preventDefault();
    var targetLink = e.target;
    this.collection.sortKey = targetLink.offsetParent.dataset.sortKey;
    this.collection.sortType = targetLink.dataset.sortType;
    this.collection.orderBy(this.collection.sortKey, this.collection.sortType);

    $('[data-sort-key!=' + this.collection.sortKey + ']>span>a').removeClass('active');
    $('[data-sort-key=' + this.collection.sortKey + ']>span>a[data-sort-type=' + this.collection.sortType + ']').addClass('active');
  }

});
