Todo.Views.TasksSearch = Backbone.View.extend({
  el: '#search',
  events: {
    "click input#submit-search" : "search"
  },
  initialize: function(){

  },

  template: function(options) {
    return JST['tasks/search'](options);
  },


  render : function() {
    return this;
  },
  search: function(e){
    var text  = $('#text-search').val();
    if(text != ''){
      this.collection.fetch({'silent': true});
      var results = this.collection.filter(function(task){
        regex = new RegExp(text)
        return task.get('title').match(regex);
      });
      this.collection.reset(results);
    }else{
      this.collection.fetch({'reset': true});
    }
  }

});
