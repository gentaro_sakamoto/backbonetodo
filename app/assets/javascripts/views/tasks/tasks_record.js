Todo.Views.TasksRecord = Backbone.View.extend({
  id: 'record',
  tagName: 'tr',
  events: {
    "dblclick td .view": "edit",
    "keypress td.title.editing .edit": "editTitle",
    "blur td.title.editing .edit": "editTitle",
    "change td.status.editing .edit": "toggleStatus",
    "blur td.status.editing .edit": "toggleStatus",
    "click td.checkbox_select input": "checkboxClicked",
    "click .delete": "destroy"

  },
  initialize: function(){
    this.listenTo(this.model, 'change', this.render);
    this.listenTo(this.model, 'sync', this.render);
    this.listenTo(this.model, 'destroy', this.remove);
  },

  template: function(options) {
    return JST['tasks/record'](options);
  },

  render : function() {
    this.$el.html(this.template({ task: this.model }));
    return this;
  },

  edit: function(e){
    $(e.currentTarget.parentNode).addClass('editing');
    //TODO:もっとよい参照の仕方があるはず、、
    $(e.currentTarget.parentNode).find('.edit')[0].lastElementChild.focus();
  },

  editTitle: function(e){
    var input = $(e.target);
    if((e.keyCode == 13 || e.type == 'focusout') && input.val()){
      this.model.save({title: input.val()}, {wait: true});
    }
  },

  toggleStatus: function(e){
    this.model.save({ status: e.target.value}, {wait: true});
  },

  checkboxClicked: function(e){
    if(e.target.checked){
      this.model.set('selected', true);
    }else{
      this.model.set('selected', false);
    }
  },

  destroy: function(e){
    this.model.destroy({wait: true});
  }

});
