Todo.Views.TasksFormRegist = Backbone.View.extend({
  el: '#form',
  events: {
    'click #button-create-task': 'create'
  },

  initialize: function(){
  },

  template: function(options) {
    return JST['tasks/form'](options);
  },

  render : function() {
    this.$el.html(this.template());
    return this;
  },

  leave: function() {
    this.off();
    this.remove();
  },

  create: function(e){
    e.preventDefault();
    this.input = this.$('#text-new-task');
    if (!this.input.val()) return;
    this.collection.create( { title: this.input.val() }, {wait: true});
    this.input.val('');
  }

});
