class TasksController < ApplicationController

  #空の場合は[]を返却する
  def index
    tasks = Task.active
    respond_to do |format|
      format.html
      format.json { render :json => tasks}
    end
  end

  respond_to :json
  #空の場合はnullを返却する
  def show
    respond_with(Task.active.find_by_id(params[:id]))
  end

  def create
    task = Task.new(params[:task])
    task.save
    respond_with(task)
  end

  def update
    task = Task.active.find_by_id(params[:id])
    task.attributes = params[:task]
    task.save
    respond_with(task)
  end

  def destroy
    task = Task.active.find_by_id(params[:id])
    if task.present?
      task.deleted = Task::DELETED
      task.save
    end
    respond_with(task)
  end
end
