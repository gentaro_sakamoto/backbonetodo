class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title, limit: 64, null: false
      t.date :end_date
      t.integer :status, default: 0, null: false                   #ステータス(0:新規, 1:進行中, 2:完了)
      t.integer :deleted, default: 0, null: false                  #削除フラグ
      t.timestamps
    end
  end
end
